from random import randint
from Utils.general import isNum

class Dados:

    def __init__(self):
        self.numeros = [1, 1, 1, 1, 1]
        self.linea1 = ''
        self.linea2 = ''
        self.linea3 = ''

    def tirar(self, reemplazo, noEsPrimera):
        if reemplazo != []:
            error = self.validarReemplazoDados(reemplazo)
            while error != '':
                if reemplazo.lower() == 'c':
                    return
                reemplazo = input(f"{error}. Debe cargar que dados cambiar (1 a 5) separados por coma (si es mas de uno), reingrese: ")
                error = self.validarReemplazoDados(reemplazo)
        if(noEsPrimera):
            for c in reemplazo.split(','):
                self.numeros[int(c) - 1] = randint(1, 6)
        else:
            for i in range(5):
                self.numeros[i] = randint(1, 6)

    def validarReemplazoDados(self, reemplazo):
        for num in reemplazo.split(','):
            if not isNum(num):
                return f"'{num}' no es un numero"
            if int(num) < 1 or int(num) > 5:
                return f"'{num}' debe estar entre el 1 y el 5"
            if len(reemplazo.split(',')) > 5:
                return f"'{reemplazo} supera la cantidad de dados"                
        return ''

    def mostrar(self):
        self.linea1 = ''
        self.linea2 = ''
        self.linea3 = ''
        for numero in self.numeros:
            self.dibujarDado(int(numero))
        print("╔═════════╗\t╔═════════╗\t╔═════════╗\t╔═════════╗\t╔═════════╗")
        print(self.linea1)
        print(self.linea2)
        print(self.linea3)
        print("╚═════════╝\t╚═════════╝\t╚═════════╝\t╚═════════╝\t╚═════════╝")

    def dibujarDado(self, dado):
        if(dado == 1):
            self.linea1 += "║         ║\t"
            self.linea2 += "║    °    ║\t"
            self.linea3 += "║         ║\t"
        elif(dado == 2):
            self.linea1 += "║  °      ║\t"
            self.linea2 += "║         ║\t"
            self.linea3 += "║      °  ║\t"
        elif(dado == 3):
            self.linea1 += "║  °      ║\t"
            self.linea2 += "║    °    ║\t"
            self.linea3 += "║      °  ║\t"
        elif(dado == 4):
            self.linea1 += "║  °   °  ║\t"
            self.linea2 += "║         ║\t"
            self.linea3 += "║  °   °  ║\t"
        elif(dado == 5):
            self.linea1 += "║  °   °  ║\t"
            self.linea2 += "║    °    ║\t"
            self.linea3 += "║  °   °  ║\t"
        elif(dado == 6):
            self.linea1 += "║  °   °  ║\t"
            self.linea2 += "║  °   °  ║\t"
            self.linea3 += "║  °   °  ║\t"
