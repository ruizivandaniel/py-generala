from Utils.general import titulo
from Utils.menu import Menu

class Combinaciones:

    def __init__(self):
        self.estado = {
            "1": "0",
            "2": "0",
            "3": "0",
            "4": "0",
            "5": "0",
            "6": "0",
            "Escalera": "0",
            "Full": "0",
            "Poker": "0",
            "Generala": "0",
            "GeneralaDoble": "0",
        }


    def chequearCombinaciones(self, numeros, bonus):
        opciones = {}

        titulo("¿Que puntaje deseas anotar?")
        
        #Numeros 1
        ptj_1 = self.contarNumero(numeros, 1)
        if(ptj_1 > 0 and self.sinPuntuar("1")):
            opciones.update({f"1-{ptj_1}": f"Anotar {ptj_1} al 1"})
        elif(self.sinPuntuar("1")):
            opciones.update({"1-X": "Tachar el 1"})

        #Numeros 2
        ptj_2 = self.contarNumero(numeros, 2)
        if(ptj_2 > 0 and self.sinPuntuar("2")):
            opciones.update({f"2-{ptj_2}": f"Anotar {ptj_2} al 2"})
        elif(self.sinPuntuar("2")):
            opciones.update({"2-X": "Tachar el 2"})

        #Numeros 3
        ptj_3 = self.contarNumero(numeros, 3)
        if(ptj_3 > 0 and self.sinPuntuar("3")):
            opciones.update({f"3-{ptj_3}": f"Anotar {ptj_3} al 3"})
        elif(self.sinPuntuar("3")):
            opciones.update({"3-X": "Tachar el 3"})

        #Numeros 4
        ptj_4 = self.contarNumero(numeros, 4)
        if(ptj_4 > 0 and self.sinPuntuar("4")):
            opciones.update({f"4-{ptj_4}": f"Anotar {ptj_4} al 4"})
        elif(self.sinPuntuar("4")):
            opciones.update({"4-X": "Tachar el 4"})

        #Numeros 5
        ptj_5 = self.contarNumero(numeros, 5)
        if(ptj_5 > 0 and self.sinPuntuar("5")):
            opciones.update({f"5-{ptj_5}": f"Anotar {ptj_5} al 5"})
        elif(self.sinPuntuar("5")):
            opciones.update({"5-X": "Tachar el 5"})

        #Numeros 6
        ptj_6 = self.contarNumero(numeros, 6)
        if(ptj_6 > 0 and self.sinPuntuar("6")):
            opciones.update({f"6-{ptj_6}": f"Anotar {ptj_6} al 6"})
        elif(self.sinPuntuar("6")):
            opciones.update({"6-X": "Tachar el 6"})

        #Escalera
        esEscalera = self.esEscalera(numeros)
        if(esEscalera and self.sinPuntuar("Escalera")):
            opciones.update({f"Escalera-{20}": f"Anotar escalera"})
        elif(self.sinPuntuar("Escalera")):
            opciones.update({"Escalera-X": "Tachar escalera."})

        #Full
        esFull = self.esFull(numeros)
        if(esFull and self.sinPuntuar("Full")):
            opciones.update({f"Full-{30}": f"Anotar full"})
        elif(self.sinPuntuar("Full")):
            opciones.update({"Full-X": "Tachar full."})

        #Poker
        esPoker = self.seRepite(numeros, 4)
        if(esPoker and self.sinPuntuar("Poker")):
            opciones.update({f"Poker-{40}": f"Anotar poker"})
        elif(self.sinPuntuar("Poker")):
            opciones.update({"Poker-X": "Tachar poker."})

        #Generala
        g1 = self.seRepite(numeros, 5)
        if(g1 and self.sinPuntuar("Generala")):
            opciones.update({f"Generala-{50}": f"ANOTAR GENERALA :D"})
        elif(self.sinPuntuar("Generala")):
            opciones.update({"Generala-X": "Tachar generala."})

        #Generala doble
        g2 = self.seRepite(numeros, 5) and not self.sinPuntuar("Generala")
        if(g2 and self.sinPuntuar("GeneralaDoble")):
            opciones.update({f"GeneralaDoble-{100}": f"ANOTAR GENERALA DOBLEEEE"})
        elif(self.sinPuntuar("GeneralaDoble")):
            opciones.update({"GeneralaDoble-X": "Tachar generala doble."})

        menu = Menu(opciones)
        opcion = menu.show()
        del menu

        self.anotarPuntaje(opcion, bonus)


    def esFull(self, numeros):
        cmc = max(set(numeros), key=numeros.count)
        return numeros.count(cmc) == 3 and len(set(numeros)) == 2


    def esEscalera(self, numeros):
        numeros.sort()
        return (numeros == [1, 2, 3, 4, 5] ) or ( numeros == [ 2, 3, 4, 5, 6] ) or ( numeros == [1, 3, 4, 5, 6])


    def contarNumero(self, numeros, num):
        return len(list(filter(lambda n: n == num, numeros))) * num


    def seRepite(self, numeros, repeticiones):
        for i in range(1, 7):
            if numeros.count(i) == repeticiones:
                return True
        return False


    def anotarPuntaje(self, key, bonus):
        (combinacion, valor) = key.split('-')
        if bonus and combinacion not in ("1", "2", "3", "4", "5", "6") and valor != 'X':
            self.estado[combinacion] = str(int(valor) + 5)
        else:
            self.estado[combinacion] = str(valor)


    def sinPuntuar(self, combinacion):
        return self.estado[str(combinacion)] == "0"


    def obtenerPuntajeTotal(self):
        total = 0
        for ptj in self.estado:
            if self.estado[ptj] != 'X':
                total += int(self.estado[ptj])
        return total

    def obtenerCombinaciones(self):
        aux = ""
        for e in self.estado:
            aux += f"{e}: {self.estado[e]}\n"
        return aux