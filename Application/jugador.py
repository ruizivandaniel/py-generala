from Application.dados import Dados
from Application.combinaciones import Combinaciones

class Jugador:

    def __init__(self, nombre):
        self.nombre = nombre
        self.combinaciones = Combinaciones()
        self.dados = Dados()

    def mostrarCombinaciones(self):
        return self.combinaciones.obtenerCombinaciones()

    def jugar(self):
        bonus = True
        for turno in range(3):
            if(turno == 0):
                self.dados.tirar([], False)
                self.dados.mostrar()
            else:
                otraVez = input("\n¿Desea tirar nuevamente? (s/n): ")
                if(otraVez.upper() == 'S'):
                    nuevaTirada = input("¿Que numeros cambiar? (c: cancelar): ")
                    if(nuevaTirada.lower() == 'c'):
                        break
                    bonus = False
                    self.dados.tirar(nuevaTirada, True)
                    self.dados.mostrar()
                else:
                    break
        self.combinaciones.chequearCombinaciones(self.dados.numeros, bonus)
