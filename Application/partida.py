from Utils.general import titulo, clearScreen, isNum
from Application.jugador import Jugador

class Partida:
    CANT_JUGADORES = 1
    JUGADORES = []

    def cantidadJugadores(self):
        auxCantJug = input("Ingrese la cantidad de jugadores: ")
        while not isNum(auxCantJug):
            auxCantJug = input("Valor incorrecto, reingrese: ")
        self.CANT_JUGADORES = int(auxCantJug)

    def cargarJugadores(self):
        clearScreen()
        self.JUGADORES = []
        print(f"Cargar nombre a los {self.CANT_JUGADORES} jugadores: ")
        for j in range(self.CANT_JUGADORES):
            nombre = input(f"\tJugador {j + 1} : ")
            while len(nombre) > 10:
                nombre = input("El largo del nombre no puede superar los 10 caracteres, reingrese: ")
            while self.existeNombre(nombre.lower().capitalize()):
                nombre = input("Ya se cargo ese nombre, reingrese: ")
            self.JUGADORES.append(Jugador(nombre.lower().capitalize()))

    def existeNombre(self, nombre):
        for jugador in self.JUGADORES:
            if jugador.nombre == nombre:
                return True
        return False

    def listarJugadores(self):
        clearScreen()
        titulo(f"Lista de los {len(self.JUGADORES)} jugadores: ")
        for jugador in self.JUGADORES:
            print(f"   ■ {jugador.nombre}")
        opcion = input("\n¿Confirmar jugadores? (s/n): ")
        while opcion.upper() != 'S':
            self.cargarJugadores()
            opcion = input("¿Confirmar jugadores? (s/n): ")

    def inicioGenerala(self):
        for _ in range(11):
            for jugadorActual in self.JUGADORES:
                clearScreen()
                titulo(f"Turno de {jugadorActual.nombre}")
                print(f"Puntaje:\n{jugadorActual.combinaciones.obtenerCombinaciones()}")
                jugadorActual.jugar()

    def finGenerala(self):
        ganador_ptj = 0
        ganador_nombre = ''
        clearScreen()
        titulo("Fin de la partida")
        l1  = "┌─────────"
        l2  = "│ JUGADOR "
        l3  = "├─────────"
        l4  = "│    1    "
        l5  = "├─────────"
        l6  = "│    2    "
        l7  = "├─────────"
        l8  = "│    3    "
        l9  = "├─────────"
        l10 = "│    4    "
        l11 = "├─────────"
        l12 = "│    5    "
        l13 = "├─────────"
        l14 = "│    6    "
        l15 = "├─────────"
        l16 = "│  ESC.   "
        l17 = "├─────────"
        l18 = "│  FULL   "
        l19 = "├─────────"
        l20 = "│  POKER  "
        l21 = "├─────────"
        l22 = "│  GEN I  "
        l23 = "├─────────"
        l24 = "│  GEN II "
        l25 = "├─────────"
        l26 = "│  TOTAL  "
        l27 = "└─────────"
        for jugador in self.JUGADORES:
            auxLineas = self.marcarLineas(jugador.nombre)
            l1  += f"┬{auxLineas}"
            l2  += f"│  {jugador.nombre}  "
            l3  += f"┼{auxLineas}"
            l4  += f"│{self.marcarEspacios(jugador.nombre, jugador.combinaciones.estado['1'])}"
            l5  += f"┼{auxLineas}"
            l6  += f"│{self.marcarEspacios(jugador.nombre, jugador.combinaciones.estado['2'])}"
            l7  += f"┼{auxLineas}"
            l8  += f"│{self.marcarEspacios(jugador.nombre, jugador.combinaciones.estado['3'])}"
            l9  += f"┼{auxLineas}"
            l10 += f"│{self.marcarEspacios(jugador.nombre, jugador.combinaciones.estado['4'])}"
            l11 += f"┼{auxLineas}"
            l12 += f"│{self.marcarEspacios(jugador.nombre, jugador.combinaciones.estado['5'])}"
            l13 += f"┼{auxLineas}"
            l14 += f"│{self.marcarEspacios(jugador.nombre, jugador.combinaciones.estado['6'])}"
            l15 += f"┼{auxLineas}"
            l16 += f"│{self.marcarEspacios(jugador.nombre, jugador.combinaciones.estado['Escalera'])}"
            l17 += f"┼{auxLineas}"
            l18 += f"│{self.marcarEspacios(jugador.nombre, jugador.combinaciones.estado['Full'])}"
            l19 += f"┼{auxLineas}"
            l20 += f"│{self.marcarEspacios(jugador.nombre, jugador.combinaciones.estado['Poker'])}"
            l21 += f"┼{auxLineas}"
            l22 += f"│{self.marcarEspacios(jugador.nombre, jugador.combinaciones.estado['Generala'])}"
            l23 += f"┼{auxLineas}"
            l24 += f"│{self.marcarEspacios(jugador.nombre, jugador.combinaciones.estado['GeneralaDoble'])}"
            l25 += f"┼{auxLineas}"
            l26 += f"│{self.marcarEspacios(jugador.nombre, jugador.combinaciones.obtenerPuntajeTotal())}"
            l27 += f"┴{auxLineas}"
        l1  += "┐"
        l2  += "│"
        l3  += "┤"
        l4  += "│"
        l5  += "┤"
        l6  += "│"
        l7  += "┤"
        l8  += "│"
        l9  += "┤"
        l10 += "│"
        l11 += "┤"
        l12 += "│"
        l13 += "┤"
        l14 += "│"
        l15 += "┤"
        l16 += "│"
        l17 += "┤"
        l18 += "│"
        l19 += "┤"
        l20 += "│"
        l21 += "┤"
        l22 += "│"
        l23 += "┤"
        l24 += "│"
        l25 += "┤"
        l26 += "│"
        l27 += "┘"

        for jugador in self.JUGADORES:
            auxPtj = jugador.combinaciones.obtenerPuntajeTotal()
            if auxPtj > ganador_ptj:
                ganador_ptj = auxPtj
                ganador_nombre = jugador.nombre

        self.mostrarGanador(ganador_nombre)

        input("\n\nPresione cualquier tecla para ver tabla de puntajes...")

        clearScreen()
        print(f"{l1}\n{l2}\n{l3}\n{l4}\n{l5}\n{l6}\n{l7}\n{l8}\n{l9}\n{l10}\n{l11}\n{l12}\n{l13}\n{l14}\n{l15}\n{l16}\n{l17}\n{l18}\n{l19}\n{l20}\n{l21}\n{l22}\n{l23}\n{l24}\n{l25}\n{l26}\n{l27}\n")


    def marcarLineas(self, nombre):
        aux = ''
        for _ in range(len(nombre)):
            aux += '─'
        return '──' + aux + '──'


    def marcarEspacios(self, nombre, valor):
        espacios = ''
        largoTotal = len(nombre) + 4
        largoValor = len(str(valor))
        for _ in range(int(largoTotal/2)):
            espacios += ' '
        resultado = espacios + str(valor) + espacios
        if largoValor%2 != 0 and largoTotal%2 != 0:
            return resultado
        elif largoValor%2 == 0 and largoTotal%2 == 0:
            return resultado[:-2]
        else:
            return resultado[:-1]


    def mostrarGanador(self, ganador):
        aux = ''
        aux += "\n            '          \     '* |    |  *        |*                *  *"
        aux += "\n                 *      `.       \   |     *     /    *      '"
        aux += "\n       .                  \      |   \          /               *"
        aux += "\n          *'  *     '      \      \   '.       |"
        aux += "\n             -._            `                  /         *"
        aux += "\n       ' '      ``._   *                           '          .      '"
        aux += "\n        *           *\*          * .   .      *                            *"
        aux += "\n       '        *    `-._                       .         _..:='        *"
        aux += "\n                  .  '      *       *    *   .       _.:--'"
        aux += "\n               *           .     .     *         .-'         *"
        aux += "\n     "
        aux += f"\n        .       *   ¡FELICITACIONES {ganador.upper()}, GANASTE!  :D      *         ."
        aux += "\n     "
        aux += "\n       *       ___.-=--..-._     *                '               '"
        aux += "\n                                       *       *"
        aux += "\n                     *        _.'  .'       `.        '  *             *"
        aux += "\n          *              *_.-'   .'            `.               *"
        aux += "\n                        .'                       `._             *  '"
        aux += "\n        '       '                        .       .  `.     ."
        aux += "\n            .                      *                  `"
        aux += "\n                    *        '             '                          ."
        aux += "\n          .                          *        .           *  *"
        aux += "\n                  *        .                                    '"
        print(aux)
