from Application.partida import Partida

print("╔═══════════════════════════════════════════════════════════════════════╗")
print("║                                                                       ║")
print("║               _                                                       ║")
print("║      __  __  |_|                                         _            ║")
print("║     |  `´  |  _       __ _  ___  _ __   ___  _ __  __ _ | | __ _      ║")
print("║     | |\/| | | |     / _` |/ _ \| '_ \ / _ \| ´ _|/ _` || |/ _` |     ║")
print("║     | |  | | | |    | (_| |  __/| | | |  __/| |    (_| || | (_| |     ║")
print("║     |_|  |_| |_|     \__, |\___||_| |_|\___||_|   \__,_||_|\__,_|     ║")
print("║                      |___/                                            ║")
print("║                                                                       ║")
print("║                                                                       ║")
print("╚═══════════════════════════════════════════════════════════════════════╝\n")

jugar = True

while jugar:
    p = Partida()
    p.cantidadJugadores()
    p.cargarJugadores()
    p.listarJugadores()
    p.inicioGenerala()
    p.finGenerala()
    jugar = input("\n\n¿Desea volver a jugar? (s/n): ")
