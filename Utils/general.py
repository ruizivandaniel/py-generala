import os

def isNum(number):
    try:
        int(number)
        return True
    except ValueError:
        return False

def clearScreen():
        clear = lambda: os.system('cls')
        clear()

def titulo(texto):
    titulo = "\n┌────────────────────────────────────────────────────────────────────────────────┐\n"
    espacios = ""
    largoTexto = len(texto)
    largo = int((80 - largoTexto) / 2)
    for _ in range(largo):
        espacios += " "
    if largoTexto % 2 == 0:
        titulo +=  "│" + espacios + texto + espacios + "│\n"
    else:
        titulo += "│" + espacios + texto + espacios + " │\n"
    titulo += "└────────────────────────────────────────────────────────────────────────────────┘\n"
    print(titulo)
