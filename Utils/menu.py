from Utils.general import clearScreen, isNum

class Menu:

    def __init__(self, opciones):
        self.opciones = opciones
        self.keys = list(opciones.keys())

    def show(self):
        aux = ''
        for (i, key) in enumerate(self.keys):
            aux += f"{i + 1}. {self.opciones[key]}\n"
        print(aux)
        op = input("Seleccione una opcion: ")
        while not isNum(op) or (int(op) < 1 or int(op) > len(self.keys)):
            op = input(f"Opcion incorrecta, reingrese una opcion entre el 1 y {len(self.keys)}: ")
        return self.keys[int(op) - 1]
